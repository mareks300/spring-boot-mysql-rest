package com.example.mysqlRest.model.enums;

public enum Title {
	Dr("Dr."), Mr("Mr."), Ms("Ms."), Prof("Prof.");

	private String label;

	private Title(String label) {
		this.label = label;
	}

	public String getId() {
		return this.name();
	}

	public String getName() {
		return this.getLabel();
	}

	public String getLabel() {
		return this.label;
	}
}
